import { createConnection } from 'typeorm'

import server from './server'

const PORT = process.env.PORT || 4000

createConnection()
  .then(() => {
    console.log('Connected to', process.env.TYPEORM_HOST)
    server.listen(PORT, () => {
      console.info(`Server listening on port ${PORT} 🚀`)
    })
  })
  .catch(e => console.log('TypeORM error:', e))
