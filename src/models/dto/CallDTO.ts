import { PatientDTO } from './PatientDTO'

export interface CallDTO extends BaseDTO {
  boxId: number
  patient: PatientDTO
  expiration: number
}
