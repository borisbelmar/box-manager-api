import { PersonDTO } from './PersonDTO'

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface DoctorDTO extends PersonDTO {}
