export { BoxDTO } from './BoxDTO'
export { CallDTO } from './CallDTO'
export { DoctorDTO } from './DoctorDTO'
export { PatientDTO } from './PatientDTO'
