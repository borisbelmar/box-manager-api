export interface PersonDTO extends BaseDTO {
  firstname: string
  lastname: string
}
