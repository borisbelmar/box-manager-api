import { PersonDTO } from './PersonDTO'

export interface PatientDTO extends PersonDTO {
  available?: boolean
}
