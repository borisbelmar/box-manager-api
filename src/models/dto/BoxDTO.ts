import { CallDTO } from './CallDTO'
import { DoctorDTO } from './DoctorDTO'

export interface BoxDTO extends BaseDTO {
  doctor?: DoctorDTO
  call?: CallDTO[]
}
