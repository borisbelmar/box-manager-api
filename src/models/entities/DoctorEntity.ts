import { Column, Entity } from 'typeorm'
import { Length } from 'class-validator'

import EntityBase from './EntityBase'
import { DoctorDTO } from '../dto'

@Entity({ name: 'doctor' })
export default class DoctorEntity extends EntityBase implements DoctorDTO {
  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  firstname!: string

  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  lastname!: string
}
