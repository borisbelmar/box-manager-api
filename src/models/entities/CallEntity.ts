import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm'
import { IsInt, IsOptional } from 'class-validator'
import { Type } from 'class-transformer'

import EntityBase from './EntityBase'
import { CallDTO } from '../dto'
import PatientEntity from './PatientEntity'
import getUnixTime from '../../utils/getUnixTime'
import { BoxEntity } from '.'

@Entity({ name: 'call' })
export default class CallEntity extends EntityBase implements CallDTO {
  @ManyToOne(() => BoxEntity, box => box.calls, { nullable: false })
  @Type(() => BoxEntity)
  @JoinColumn({ name: 'boxId' })
  box!: BoxEntity

  @Column({ type: 'int' })
  @IsInt()
  boxId!: number

  @ManyToOne(() => PatientEntity, patient => patient.calls, { nullable: false })
  @Type(() => PatientEntity)
  @JoinColumn()
  patient!: PatientEntity

  @Column({ type: 'int' })
  @IsInt()
  @IsOptional()
  expiration = getUnixTime() + (60 * 20)
}
