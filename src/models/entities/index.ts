export { default as BoxEntity } from './BoxEntity'
export { default as CallEntity } from './CallEntity'
export { default as PatientEntity } from './PatientEntity'
export { default as DoctorEntity } from './DoctorEntity'
