import { Entity, JoinColumn, OneToMany, OneToOne, Unique } from 'typeorm'
import { IsOptional } from 'class-validator'
import { Type } from 'class-transformer'

import EntityBase from './EntityBase'
import { BoxDTO } from '../dto'
import DoctorEntity from './DoctorEntity'
import CallEntity from './CallEntity'

@Entity({ name: 'box' })
@Unique('doctor_unique', ['doctor'])
export default class BoxEntity extends EntityBase implements BoxDTO {
  @OneToOne(() => DoctorEntity, { eager: true })
  @Type(() => DoctorEntity)
  @JoinColumn()
  doctor!: DoctorEntity

  @OneToMany(() => CallEntity, call => call.box, { eager: true, onDelete: 'CASCADE' })
  @Type(() => CallEntity)
  @IsOptional()
  calls?: CallEntity[]
}
