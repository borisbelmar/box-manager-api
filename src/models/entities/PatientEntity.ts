import { Column, Entity, JoinColumn, OneToMany } from 'typeorm'
import { IsOptional, Length } from 'class-validator'

import EntityBase from './EntityBase'
import { PatientDTO } from '../dto'
import { CallEntity } from '.'

@Entity({ name: 'patient' })
export default class PatientEntity extends EntityBase implements PatientDTO {
  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  firstname!: string

  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  lastname!: string

  @OneToMany(() => CallEntity, call => call.patient)
  @IsOptional()
  @JoinColumn()
  calls?: PatientEntity
}
