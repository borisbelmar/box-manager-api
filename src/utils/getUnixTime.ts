function getUnixTime (): number {
  return Math.floor(Date.now() / 1000)
}

export default getUnixTime
