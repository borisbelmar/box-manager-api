import Router from '@koa/router'

import boxRoutes from './boxRoutes'
import callRoutes from './callRoutes'
import patientRoutes from './patientRoutes'
import doctorRoutes from './doctorRoutes'

const router = new Router()

router.use('/boxes', boxRoutes.routes(), boxRoutes.allowedMethods())
router.use('/calls', callRoutes.routes(), callRoutes.allowedMethods())
router.use('/patients', patientRoutes.routes(), patientRoutes.allowedMethods())
router.use('/doctors', doctorRoutes.routes(), doctorRoutes.allowedMethods())

export default router
