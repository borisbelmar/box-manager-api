import Router from '@koa/router'
import { BoxController } from '../controllers'

const router: Router = new Router()
const controller = new BoxController()

router
  .get('/', controller.getAll)
  .get('/currentCall', controller.getAllWithCurrentCall)
  .post('/', controller.register)
  .delete('/:boxId(^[0-9]+$)', controller.delete)

export default router
