import Router from '@koa/router'
import { CallController } from '../controllers'

const router: Router = new Router()
const controller = new CallController()

router
  .get('/', controller.getAll)
  .post('/', controller.register)
  .delete('/:callId', controller.delete)

export default router
