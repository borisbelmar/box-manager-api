import Router from '@koa/router'
import { DoctorController } from '../controllers'

const router: Router = new Router()
const controller = new DoctorController()

router
  .get('/', controller.getAll)
  .post('/', controller.register)
  .delete('/:patientId(^[0-9]+$)', controller.delete)

export default router
