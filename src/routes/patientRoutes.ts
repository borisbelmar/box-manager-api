import Router from '@koa/router'
import { PatientController } from '../controllers'

const router: Router = new Router()
const controller = new PatientController()

router
  .get('/', controller.getAll)
  .post('/', controller.register)
  .delete('/:patientId(^[0-9]+$)', controller.delete)

export default router
