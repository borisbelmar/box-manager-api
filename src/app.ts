import Koa from 'koa'
import koaBody from 'koa-body'
import cors from '@koa/cors'

import responseTime from './middlewares/responseTime'
import logger from './middlewares/logger'
import errorHandler from './middlewares/errorHandler'

import router from './routes'

// Init
const app: Koa = new Koa()

// Middlewares
app.use(logger)
app.use(responseTime)
app.use(koaBody({ jsonLimit: '1kb' }))
app.use(cors())
app.use(errorHandler)

// Routes
app.use(router.routes())
app.use(router.allowedMethods())

export default app
