export { default as BoxRepository } from './BoxRepository'
export { default as CallRepository } from './CallRepository'
export { default as DoctorRepository } from './DoctorRepository'
export { default as PatientRepository } from './PatientRepository'
