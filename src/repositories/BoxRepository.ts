import { DeleteResult, getConnection, InsertResult } from 'typeorm'
import { BoxEntity } from '../models/entities'
import getUnixTime from '../utils/getUnixTime'

export default class BoxRepository {
  public getAll (): Promise<BoxEntity[]> {
    return getConnection().getRepository(BoxEntity).find({ relations: ['doctor'] })
  }

  public create (box: BoxEntity): Promise<InsertResult> {
    return getConnection().getRepository(BoxEntity).insert(box)
  }

  public delete (boxId: number): Promise<DeleteResult> {
    return getConnection().getRepository(BoxEntity).delete(boxId)
  }

  public getAllWithCurrentCall (): Promise<BoxEntity[]> {
    return getConnection()
      .getRepository(BoxEntity)
      .createQueryBuilder('box')
      .leftJoinAndSelect('box.doctor', 'doctor')
      .leftJoinAndSelect('box.calls', 'call', 'call.expiration > :now', { now: getUnixTime() })
      .leftJoinAndSelect('call.patient', 'patient')
      .orderBy('call.expiration', 'DESC')
      .getMany()
  }
}
