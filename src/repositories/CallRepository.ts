import { DeleteResult, getConnection, InsertResult } from 'typeorm'
import { CallEntity } from '../models/entities'
import getUnixTime from '../utils/getUnixTime'

export default class CallRepository {
  public getAll (): Promise<CallEntity[]> {
    return getConnection().getRepository(CallEntity).find({ relations: ['patient'] })
  }

  public create (call: CallEntity): Promise<InsertResult> {
    return getConnection().getRepository(CallEntity).insert(call)
  }

  public delete (callId: number): Promise<DeleteResult> {
    return getConnection().getRepository(CallEntity).delete(callId)
  }

  public async isAvailable (boxId: number, patientId: number): Promise<boolean> {
    const count = await getConnection()
      .getRepository(CallEntity)
      .createQueryBuilder('call')
      .innerJoin('call.box', 'box')
      .innerJoin('call.patient', 'patient')
      .where('call.expiration > :now AND box.id = :boxId', { now: getUnixTime(), boxId })
      .orWhere('call.expiration > :now AND patient.id = :patientId', { now: getUnixTime(), patientId })
      .getCount()
    return count === 0
  }
}
