import { DeleteResult, getConnection, InsertResult } from 'typeorm'
import { PatientEntity } from '../models/entities'
import getUnixTime from '../utils/getUnixTime'

export default class PatientRepository {
  public getAll (): Promise<PatientEntity[]> {
    return getConnection().getRepository(PatientEntity).find()
  }

  public create (patient: PatientEntity): Promise<InsertResult> {
    return getConnection().getRepository(PatientEntity).insert(patient)
  }

  public delete (patientId: number): Promise<DeleteResult> {
    return getConnection().getRepository(PatientEntity).delete(patientId)
  }

  public async hasCurrentCall (patientId: number): Promise<boolean> {
    const count = await getConnection()
      .getRepository(PatientRepository)
      .createQueryBuilder('patient')
      .innerJoin('patient.calls', 'call')
      .where('call.expiration > :now AND patient.id = :patientId', { now: getUnixTime(), patientId })
      .getCount()
    return count > 0
  }
}
