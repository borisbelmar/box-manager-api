import { DeleteResult, getConnection, InsertResult } from 'typeorm'
import { DoctorEntity } from '../models/entities'

export default class DoctorRepository {
  public getAll (): Promise<DoctorEntity[]> {
    return getConnection().getRepository(DoctorEntity).find()
  }

  public create (doctor: DoctorEntity): Promise<InsertResult> {
    return getConnection().getRepository(DoctorEntity).insert(doctor)
  }

  public delete (doctorId: number): Promise<DeleteResult> {
    return getConnection().getRepository(DoctorEntity).delete(doctorId)
  }
}
