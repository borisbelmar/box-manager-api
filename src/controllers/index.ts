export { default as BoxController } from './BoxController'
export { default as CallController } from './CallController'
export { default as DoctorController } from './DoctorController'
export { default as PatientController } from './PatientController'
