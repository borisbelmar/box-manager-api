import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { isEmpty } from 'ramda'
import { Context } from 'koa'
import { DoctorEntity } from '../models/entities'
import { DoctorRepository } from '../repositories'

const repository = new DoctorRepository()

export default class DoctorController {
  async getAll (ctx:Context): Promise<void> {
    const doctors = await repository.getAll()
    ctx.body = doctors
  }

  async register (ctx:Context): Promise<void> {
    const newDoctor = plainToClass(DoctorEntity, ctx.request.body)
    const errors = await validate(newDoctor)
    if (isEmpty(errors)) {
      try {
        const data = await repository.create(newDoctor)
        ctx.status = 201
        ctx.body = data
      } catch (err) {
        if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
        if (err.errno === 1062) {
          ctx.throw(409)
        } else {
          ctx.throw(500)
        }
      }
    } else {
      ctx.throw(400)
    }
  }

  async delete (ctx:Context): Promise<void> {
    ctx.body = 'Call remove'
  }
}
