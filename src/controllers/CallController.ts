import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { Context } from 'koa'
import { isEmpty } from 'ramda'
import { CallEntity } from '../models/entities'
import { CallRepository } from '../repositories'

const repository = new CallRepository()

export default class CallController {
  async getAll (ctx:Context): Promise<void> {
    const calls = await repository.getAll()
    ctx.body = calls
  }

  async register (ctx:Context): Promise<void> {
    const newCall = plainToClass(CallEntity, ctx.request.body)
    const errors = await validate(newCall)
    if (isEmpty(errors)) {
      const isAvailable = await repository.isAvailable(newCall.boxId, newCall.patient.id as number)
      console.log(isAvailable)
      if (isAvailable) {
        try {
          const data = await repository.create(newCall)
          ctx.status = 201
          ctx.body = data
        } catch (e) {
          if (e.errno === 1062) {
            ctx.throw(409, 'El llamado ya existe')
          } else if (e.errno === 1452) {
            ctx.throw(409, 'El paciente no existe')
          } else {
            ctx.throw(500)
          }
        }
      } else {
        ctx.throw(409, 'El box o el paciente ya están presentes en un llamado')
      }
    } else {
      console.log(400, errors)
      ctx.throw(400, errors)
    }
  }

  async delete (ctx:Context): Promise<void> {
    const callId = ctx.params.callId
    const res = await repository.delete(callId)
    if (res.affected && res.affected > 0) {
      ctx.status = 204
    } else {
      ctx.throw(404, 'El llamado que se intenta cancelar no existe')
    }
  }
}
