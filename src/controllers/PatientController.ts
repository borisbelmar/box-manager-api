import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { isEmpty } from 'ramda'
import { Context } from 'koa'
import { PatientEntity } from '../models/entities'
import { PatientRepository } from '../repositories'

const repository = new PatientRepository()

export default class PatientController {
  async getAll (ctx:Context): Promise<void> {
    const patients = await repository.getAll()
    ctx.body = patients
  }

  async register (ctx:Context): Promise<void> {
    const newCall = plainToClass(PatientEntity, ctx.request.body)
    const errors = await validate(newCall)
    if (isEmpty(errors)) {
      try {
        const data = await repository.create(newCall)
        ctx.status = 201
        ctx.body = data
      } catch (err) {
        if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
        if (err.errno === 1062) {
          ctx.throw(409)
        } else {
          ctx.throw(500)
        }
      }
    } else {
      ctx.throw(400)
    }
  }

  async delete (ctx:Context): Promise<void> {
    ctx.body = 'Call remove'
  }
}
