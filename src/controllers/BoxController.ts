import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { isEmpty } from 'ramda'
import { Context } from 'koa'
import { BoxEntity } from '../models/entities'
import { BoxRepository } from '../repositories'

const repository = new BoxRepository()

export default class BoxController {
  async getAll (ctx:Context): Promise<void> {
    const boxes = await repository.getAll()
    ctx.body = boxes
  }

  async register (ctx:Context): Promise<void> {
    const newBox = plainToClass(BoxEntity, ctx.request.body)
    const errors = await validate(newBox)
    if (isEmpty(errors)) {
      try {
        const data = await repository.create(newBox)
        ctx.status = 201
        ctx.body = data
      } catch (e) {
        if (process.env.NODE_ENV !== 'test') { console.log(e.errno, e.message) }
        if (e.errno === 1062) {
          ctx.throw(409)
        } else if (e.errno === 1452) {
          ctx.throw(409, 'El doctor no existe')
        } else {
          ctx.throw(500)
        }
      }
    } else {
      console.log(400, errors)
      ctx.throw(400, errors)
    }
  }

  async delete (ctx:Context): Promise<void> {
    ctx.body = 'Call remove'
  }

  async getAllWithCurrentCall (ctx:Context): Promise<void> {
    const boxes = await repository.getAllWithCurrentCall()
    ctx.body = boxes
  }
}
