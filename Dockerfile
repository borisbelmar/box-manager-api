FROM node:12-alpine

COPY ["package.json", "yarn.lock", "tsconfig.json", "ormconfig.js", "/app/"]

WORKDIR "/app"

RUN "yarn"

COPY ["./src/", "./src"]

EXPOSE 4000

CMD ["yarn", "dev"]